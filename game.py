import pygame
import sys
import os
import random
pygame.init()
HEIGHT=400
WIDTH=400
TITTLE="scorpion"
FPS=30
BLACK=(0,0,0)
WHITE=(255,255,255)
RED=(255,0,0)
YELLOW=(140,40,0)
gameDisplay=pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption(TITTLE)
clock=pygame.time.Clock()
GameEnd=False
GAME_FOLDER=os.path.dirname(__file__)
img_dir=os.path.join(GAME_FOLDER,"img")
back=pygame.image.load(os.path.join(img_dir,"background.png")).convert()
scorpion_img=pygame.image.load(os.path.join(img_dir,"scorpion.png")).convert()
scorpion_img=pygame.transform.scale(scorpion_img,(90,80))
scorpion_img.set_colorkey((WHITE))
"""enemy_img=pygame.image.load(os.path.join(img_dir,"enemy.png")).convert()
enemy_img=pygame.transform.scale(enemy_img,(60,40))
enemy_img.set_colorkey((WHITE))"""
enemies=[]
enemies_img=["enemy1.png","enemy2.png","enemy3.png"]
for number,enemy in enumerate(enemies_img):
	enemies.append(pygame.image.load(os.path.join(img_dir,enemy)).convert())
	enemies[number].set_colorkey((WHITE))
bullet_img=pygame.image.load(os.path.join(img_dir,"fuego.png")).convert()
bullet_img=pygame.transform.scale(bullet_img,(30,30))
bullet_img.set_colorkey((BLACK))
back_rect=back.get_rect()
def draw_text(surf,txt,size,x,y,font_name):
	font=pygame.font.Font(font_name,size)
	text=font.render(txt,True,WHITE)
	font_rect=text.get_rect()
	font_rect.midtop=(x,y)
	surf.blit(text,font_rect)
class Mob(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.image=random.choice(enemies)
		self.image=pygame.transform.scale(self.image,(random.randrange(50,80),random.randrange(40,70)))
		self.rect=self.image.get_rect()
		self.radius=int((self.rect.width /2 )*0.78)
		#pygame.draw.circle(self.image,YELLOW,self.rect.center,self.radius)
		self.rect.x=WIDTH+self.rect.width
		self.rect.y=random.randrange(0,HEIGHT)
		self.speedx=-(random.randrange(1,8))
		self.speedy=random.randrange(-1,3)
	def borders(self):
		if self.rect.left<=0 or self.rect.top>=HEIGHT or self.rect.bottom<=0:
				self.rect.y=random.randrange(0,HEIGHT)
				self.rect.x=random.randrange(WIDTH,WIDTH+10)
	def update(self):
		self.rect.x+=self.speedx
		self.rect.y-=self.speedy
		self.borders()
class Bullet(pygame.sprite.Sprite):
	def __init__(self,x,y):
		pygame.sprite.Sprite.__init__(self)
		self.image=bullet_img
		self.rect=self.image.get_rect()
		self.rect.right=x
		self.rect.bottom=y
		self.speedx=10
	def update(self):
		self.rect.x+=self.speedx
		if self.rect.right==WIDTH:
			self.kill()
class Player(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)#default
		self.image=scorpion_img
		self.rect=self.image.get_rect()
		self.radius=int((self.rect.width/2)*0.88)
		#pygame.draw.circle(self.image,YELLOW,self.rect.center,self.radius)
		self.rect.centery=HEIGHT/2
		self.rect.left=10
		self.speedy=0
		self.speedx=0
	def update(self):
		self.speedy=0
		self.speedx=0
		keystate=pygame.key.get_pressed()
		if keystate[pygame.K_w]:
			self.speedy=-5
		if keystate[pygame.K_s]:
			self.speedy=5
		if keystate[pygame.K_d]:
			self.speedx=5
		if keystate[pygame.K_a]:
			self.speedx=-5
		self.borders()
		self.rect.y+=self.speedy
		self.rect.x+=self.speedx
	def shoot(self):
		bullet=Bullet(self.rect.right, self.rect.centery)
		all_sprites.add(bullet)
		bullets.add(bullet)
	def borders(self):
		if self.rect.top<0:
			self.rect.top=0
		if self.rect.bottom>HEIGHT:
			self.rect.bottom=HEIGHT
		if self.rect.left<0:
			self.rect.left=0
		if self.rect.right>WIDTH:
			self.rect.right=WIDTH
mobs=pygame.sprite.Group()
font=pygame.font.SysFont(None,25)
all_sprites=pygame.sprite.Group()
player=Player()
all_sprites.add(player)
bullets=pygame.sprite.Group()
score=0
for w in range(8):
	m=Mob()
	all_sprites.add(m)
	mobs.add(m)
while(GameEnd==False):
	clock.tick(FPS)
	gameDisplay.blit(back,back_rect)	#main game loop
	for event in pygame.event.get():
		if event.type==pygame.QUIT:
			GameEnd=False
			pygame.quit()#loop throw all the events
			sys.exit
		if event.type==pygame.KEYDOWN:
			if event.key==pygame.K_SPACE:
				player.shoot()
		if event.type==pygame.KEYDOWN:
			if event.key==pygame.K_p:
				f=0
				while f==0:
					for event in pygame.event.get():
						if event.type==pygame.KEYDOWN:
							if event.key==pygame.K_i:
								f=1
							if event.type == pygame.QUIT:
								pygame.quit()
								quit()
	hits=pygame.sprite.groupcollide(bullets,mobs,True,True)
	for hit in hits:
		score+=1
		m=Mob()
		all_sprites.add(m)
		mobs.add(m)
	hits=pygame.sprite.spritecollide(player,mobs,False,pygame.sprite.collide_circle)#colition player with mob
	if hits:
		GameEnd=True
	all_sprites.update()
	all_sprites.draw(gameDisplay)
	draw_text(gameDisplay,str(score),30,WIDTH/2,10,pygame.font.match_font("Arial"))
	pygame.display.flip()
